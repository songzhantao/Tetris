package edu.taru.app;

import java.awt.Color;
import java.awt.Graphics;

/**
 * 图形类
 * @author 
 *
 */
public class Shape {
	Color  color; //颜色
	protected int x; //格子坐标
	protected  int y;//格子坐标
	public  static  final  int WIDTH=30; //组成图形方格的宽度px
	int [][] values ;
	public   Shape(int [][] values){
		this.values=values;
	}

	/**
	 * 下落
	 */
	public void down(){
		System.out.println(this);
		 y++;
	}
	/**
	 * 向左
	 */
	public void left(){
		 x--;
	}
	/**
	 * 向右
	 */
	public void right(){
		x++;
	}
	
	/**
	 * 旋转（向上）  比较复杂
	 */
	public void rotate(){
		 if(values!=null){
			 this.values=Utils.rotate(this.values);
		 }
		
	}

	
	/**
	 * 绘制自己,子类需要重写
	 */
	public  void paint(Graphics g){
		g.setColor(this.color);
		for(int i=0;i<values.length;i++){
			for(int k=0;k<values[i].length;k++){
				if(values[i][k]==1){
					g.fillRect((k+x)*Shape.WIDTH, (i+y)*Shape.WIDTH, Shape.WIDTH,Shape.WIDTH);
				}
			}
		}
	}
	
	@Override
	public String toString() {
		return "[x:"+x+",y="+y+"]";
	}

	
}
