package edu.taru.app;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class StageJpanel extends JPanel    {
	
	int width=15;  //场景的宽度是多少个格子
	int height=20; //高度是多少个格子
	
	//图形容器
	int [][] container =new  int [height][width];
	//正在跳动的图形
	private Shape   shape=null;  
	//是否允许自动下落
	boolean  isAutoFall =true;
	//控制图形的线程
	Thread  t =new Thread(new Runnable() {
		
		@Override
		public void run() {
			while(isAutoFall){
				//下落
				if(shape!=null){
					if(isDown()){
						shape.down();
						repaint();
					}else{
						if(isAlive()){
							//把图形复制到已经掉落的图形库中
							fill();
							shape=null;
						}else{
							//停止线程
							isAutoFall=false;
							JOptionPane.showMessageDialog(StageJpanel.this, "游戏结束");
						}
					
						
						
					}
			
				}else{
					shape =	ShapeFactory.randomShape();
					repaint();
				}
				
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		
		}
	});
	//键盘监听器
	KeyListener  keyListener =new KeyListener() {
		@Override
		public void keyTyped(KeyEvent e) {}
		@Override
		public void keyReleased(KeyEvent e) {	}
		//按键盘的四个方向键的时候
		@Override
		public void keyPressed(KeyEvent e) {
		
				switch (e.getKeyCode()) {
				case  KeyEvent.VK_LEFT :
					if(isLeft()){
						shape.left();
					}
					break;
				case  KeyEvent.VK_RIGHT :
					if(isRight()){
						shape.right();
					}
				
					break;
				case  KeyEvent.VK_UP :
					//shape.rotate();
					break;
				case  KeyEvent.VK_DOWN :
					shape.down();
					break;		
				}
				//立刻重绘
				repaint();
			
			
		}
	};
	
	/**
	 * 构造方法
	 */
	public  StageJpanel(){
		//添加键盘监听
		addKeyListener(keyListener);
	}
	
	
	/**
	 * 设置当前掉落的图形
	 * @param shape
	 */
	public   void setShape(Shape shape){
		this.shape=shape;
		
	}
	/**
	 * 启动线程控制图形掉落
	 */
	public void   start(){
		t.start();
	}
	/**
	 * 碰撞检测之是否可以左边
	 * @return
	 */
	@Deprecated  //有bug
	public   boolean  isLeft(){
		boolean  bool=false;
		if(shape.x>0){
			bool=true;
		}
		return bool;
	}
	/**
	 * 碰撞检测之是否可以右边
	 * @return
	 */
	@Deprecated //有bug
	public   boolean  isRight(){
		boolean  bool=false;
		return bool;
	}
	
	/**
	 * 碰撞检测之是否下边，如果不能下边图形就要死掉
	 * @return
	 */
	public   boolean  isDown(){
		boolean  bool=true;
		for(int i=0;i<shape.values.length;i++){
			for(int j=0;j<shape.values[i].length;j++){
				if(shape.values[i][j]==1){
						//边界
						if(shape.y+i>=height-1){
							return false;
						}
						
						//判断是否碰到其他图形
						if(container[shape.y+i+1][shape.x+j]==1){
							return false;
						}
							
				}
			}
				
		}
			
		return bool;
	}
	
	
	
	/**
	 * 把刚刚掉落的图形添加到死亡容器
	 * 
	 */
	private   void  fill(){
		for(int i =0;i<shape.values.length;i++){
			for(int k =0;k<shape.values[i].length;k++){
				if(shape.values[i][k]==1){
					container[shape.y+i][shape.x+k]=shape.values[i][k];
				}
			}
		}
	}
	
	
	
	/**
	 * 绘图
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g); //初始化默认值
		//绘制背景
		for(int i=0;i<=width;i++){
			g.drawLine(i*Shape.WIDTH, 0, i*Shape.WIDTH, height*Shape.WIDTH);
		}
		for(int i=0;i<=height;i++){
			g.drawLine(0, i*Shape.WIDTH, width*Shape.WIDTH, i*Shape.WIDTH);
		}
		//绘制已经死亡的图形
		for(int i=0;i<container.length;i++){
			for(int k=0;k<container[i].length;k++){
				if(container[i][k]==1){
					g.fillRect((k)*Shape.WIDTH, (i)*Shape.WIDTH, Shape.WIDTH,Shape.WIDTH);
				}
			}
		}
		
		
	
		
		if(shape!=null){
			shape.paint(g); //绘制图形
		}
		
	}
	
	public  boolean  isAlive(){
		if(shape.y==0){
			return false;
		}
		return true;
	}
	
	  
}
