package edu.taru.app;
import javax.swing.JFrame;

/**
 * 游戏启动界面  
 * @author songzt
 *
 */
public class Application extends JFrame {
	StageJpanel stageJpanel=new StageJpanel();
	
	public Application(){
		this.add(stageJpanel);
		this.setSize(600,700);
		this.setTitle("俄罗斯方块 ");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		stageJpanel.requestFocus();  //获取焦点否则无法监听事件
	}
	/**
	 * 启动游戏
	 */
	public  void  start(){
		//通过工厂初始化图形
		Shape shape =ShapeFactory.randomShape();
		stageJpanel.setShape(shape);
		stageJpanel.start();
		
	}
	
	
	public static void main(String[] args) {
		Application  tetris =new  Application();
		tetris.start();
	}
}
