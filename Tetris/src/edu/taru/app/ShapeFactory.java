package edu.taru.app;

import java.util.Random;

/**
 * 图形工厂负责生产图形
 * @author szt
 *
 */
public class ShapeFactory {
	//
	private  static  int [][][] shapes ={
			{
				{1,1,0},
				{1,1,0},
				{0,0,0}
			},
			{
				{1,0,0},
				{1,0,0},
				{1,1,0}
			}
			,
			{
				{1,0,0},
				{1,0,0},
				{1,0,0}
			}
			,
			{
				{0,1,0},
				{1,1,1},
				{0,0,0}
			}
	
	};
	
	/**
	 * 随机产生一个图形
	 * @return
	 */
	public static  Shape  randomShape(){
		Random random =new Random();
		int i =random.nextInt(shapes.length);
		Shape  shape =new Shape(shapes[i]);
		return  shape;
	}
	
	
	
	
}
