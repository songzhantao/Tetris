package edu.taru.app;
/**
 * 工具类
 * @author Administrator
 *
 */
public class Utils {

	/**
	 * 旋转算法
	 * @param matrix
	 * @return
	 */
	 public static int[][] rotate(int [][]matrix){  
	        int [][]temp=new int[matrix[0].length][matrix.length];  
	        int dst=matrix.length-1;  
	        for(int i=0;i<matrix.length;i++,dst--){  
	            for(int j=0;j<matrix[0].length;j++){  
	                temp[j][dst]=matrix[i][j];  
	            }  
	        }  
	        return temp;  
	    }  
	      
	
}
